Task: Server
Install: true
Description: Bio-Linux server
 This metapackage installs packages previously distributed via Bio-Linux that
 may particularly appeal to servers.

Suggests: elph

Suggests: lucy

Suggests: caftools

Recommends: bioperl, bioperl-run, libncbi6-dev

Recommends: mcl
X-Category: Analysis of network structure
X-Importance: Clustering of graphs

Recommends: biosquid

Recommends: cwltool

Recommends: libvibrant6-dev

Recommends: python3-biopython | python-biopython

Suggests: python3-biopython-sql | python-biopython-sql, python-biopython-doc

Recommends: python-cogent

Recommends: python-screed

Recommends: python3-cutadapt | python-cutadapt

Recommends: ruby-bio

Recommends: libbiojava-java, libbiojava4-java

Recommends: libgenome-dev, libmuscle-dev, libmems-dev

Recommends: libhtsjdk-java

Recommends: libai-fann-perl

Recommends: libbio-mage-perl
Why: Useful for the submission of microarray data to public repositories.

Recommends: libnhgri-blastall-perl

Recommends: libtfbs-perl

Recommends: libfreecontact-dev, libfreecontact-perl

Suggests: libfreecontact-doc

Recommends: libgo-perl

Recommends: r-cran-genetics, r-cran-haplo.stats, r-cran-phylobase

Recommends: r-cran-rncl, r-cran-rnexml

Recommends: libbio-graphics-perl, libbio-coordinate-perl

Recommends: libbio-primerdesigner-perl

Recommends: libace-perl

Recommends: libbiococoa-dev

Recommends: libstaden-read-dev

Recommends: libsrf-dev

Recommends: libzerg0-dev, libzerg-perl

Recommends: librg-reprof-bundle-perl

Recommends: python-corepywrap, librcsb-core-wrapper0-dev

Recommends: libsbml5-dev, sbmltoolbox

Suggests: r-cran-rocr
Why: Even if this package is not directly connected to biology it is maintained by
     Debian Med team and should be in our focus and it is not wrong to suggest this
     package for development of biological applications based on R

Recommends: seqan-dev

Recommends: libbio-mage-utils-perl

Recommends: libchado-perl

Recommends: libpal-java

Recommends: libbioparser-dev, libspoa-dev, libedlib-dev, libthread-pool-dev

Recommends: libjebl2-java
Remark: Fork from jebl
 This is a branch of the original JEBL on
 http://sourceforge.net/projects/jebl/ to develop a new API and class
 library.

Suggests: r-bioc-affy,
          r-bioc-affyio,
          r-bioc-altcdfenvs,
          r-bioc-annotationdbi,
          r-bioc-biocgenerics,
          r-bioc-biomart,
          r-bioc-biomformat,
          r-bioc-biovizbase,
          r-bioc-bsgenome,
          r-bioc-genomeinfodb,
          r-bioc-genomicalignments,
          r-bioc-genomicfeatures,
          r-bioc-genomicranges,
          r-bioc-graph,
          r-bioc-hypergraph,
          r-bioc-iranges,
          r-bioc-makecdfenv,
          r-bioc-preprocesscore,
          r-bioc-rbgl,
          r-bioc-rsamtools,
          r-bioc-shortread,
          r-bioc-snpstats,
          r-bioc-variantannotation,
          r-bioc-xvector

Suggests: r-cran-rentrez

Suggests: ruby-rgfa

Recommends: r-bioc-biobase
Remark: This is a part of Bioconductor project
 A nice overview about all modules of BioDonductor is given at
 http://www.bioconductor.org/packages/release/bioc/

Recommends: libffindex0-dev

Recommends: librostlab3-dev

Suggests: librostlab-doc

Recommends: librostlab-blast0-dev

Suggests: librostlab-blast-doc

Recommends: librg-blast-parser-perl

Recommends: libsort-key-top-perl

Recommends: libhmsbeagle-dev

Recommends: libforester-java

X-Mark: Prospective packages are starting here.

X-Mark: Packages in Vcs - Information about these is queried from UDD as well

Recommends: libbambamc-dev

Recommends: libbamtools-dev

Recommends: libpbbam-dev

Recommends: libbio-das-lite-perl

Recommends: python-mmtk
Language: C, Python

Recommends: libopenms-dev

Recommends: libgenometools0-dev

Recommends: librdp-taxonomy-tree-java

Recommends: python-biom-format

Recommends: python-rdkit

Suggests: libswarm2-dev

Recommends: libgenome-perl, libgenome-model-tools-music-perl

Recommends: pyfai

Recommends: libhts-dev

Recommends: python-htseq

Recommends: python3-intervaltree-bio | python-intervaltree-bio

Recommends: python3-csb | python-csb

Recommends: python3-misopy | python-misopy

Recommends: python-freecontact

Recommends: python3-pymummer

Suggests: libgtextutils-dev

Recommends: libkmer-dev

Recommends: libsnp-sites1-dev

Recommends: libssm-dev

Recommends: librelion-dev

Recommends: libdivsufsort-dev

Recommends: bioclipse
Homepage: http://www.bioclipse.net/
License: Eclipse Public License (EPL) + exception
Pkg-Description: platform for chemo- and bioinformatics based on Eclipse
 The Bioclipse project is aimed at creating a Java-based, open source,
 visual platform for chemo- and bioinformatics based on the Eclipse
 Rich Client Platform (RCP). Bioclipse, as any RCP application, is based
 on a plugin architecture that inherits basic functionality and visual
 interfaces from Eclipse, such as help system, software updates,
 preferences, cross-platform deployment etc.
 .
 Bioclipse will provide functionality for chemo- and bioinformatics,
 and extension points that easily can be extended by plugins to provide
 added functionality. The first version of Bioclipse includes a
 CDK-plugin (bc_cdk) to provide a chemoinformatic backend, a Jmol-plugin
 (bc_jmol) for 3D-visualization and a general logging plugin. To stay
 updated on upcoming features, releases, new plugins etc, please register
 for the mailing list bioclipse-announce. The development is best
 followed on the Bioclipse Wiki where we document the progress and
 ideas of the development on a daily basis.

Recommends: libgff-dev

Recommends: python-pysam

Recommends: python-pbcore, python-pbh5tools

Recommends: python-cobra

Recommends: libtabixpp-dev

Recommends: python3-ruffus | python-ruffus

Recommends: python3-hyphy | python-hyphy

Recommends: python3-dendropy | python-dendropy

Recommends: python3-skbio

Recommends: python3-pbconsensuscore | python-pbconsensuscore

Recommends: python3-consensuscore2 | python-consensuscore2, libconsensuscore-dev

Recommends: python-pbcommand

Recommends: python3-pyvcf | python-pyvcf

Recommends: python3-pyfaidx | python-pyfaidx

Recommends: python-kineticstools

Recommends: python3-pyfasta | python-pyfasta
Remark: Testsuite uncovers problems and code is not actively maintained
 The upstream author of python-pyfasta confirmed that he stopped
 the development and recommends python-pyfaidx instead.  So the
 packaging code for python-pyfasta in SVN works but there is no
 intend to really upload the package.

Suggests: libbam-dev

Recommends: libqes-dev

Recommends: libfast5-dev, python3-fast5 | python-fast5

Recommends: libpbseq-dev, libpbdata-dev, libpbihdf-dev, libblasr-dev

Suggests: libpbcopper-dev

Recommends: libminimap-dev

Recommends: libncl-dev

Recommends: libngs-sdk-dev, libngs-java, python3-ngs | python-ngs, libncbi-vdb-dev

Recommends: libqcpp-dev

Recommends: libbpp-core-dev, libbpp-phyl-dev, libbpp-phyl-omics-dev, libbpp-popgen-dev, libbpp-qt-dev, libbpp-raa-dev, libbpp-seq-dev, libbpp-seq-omics-dev

Recommends: octace-bioinfo
Homepage: http://octave.sourceforge.net/bioinfo/index.html
License: GPL-2+
Pkg-Description: Bioinformatics manipulation for Octave
  aa2int:
    Convert amino acid characters into integers.
  aminolookup:
    Convert between amino acid representations.
  cleave:
    Cleave a peptide SEQUENCE using the PATTERN at the POSITION relative to the pattern.
  int2aa
    Convert amino acid integers into characters.
  seqreverse
    Reverse a nucleotide sequence.

Recommends: ruby-crb-blast

Recommends: libsmithwaterman-dev

Recommends: libfastahack-dev

Recommends: mgltools-networkeditor, mgltools-vision, mgltools-pybabel

Suggests: libdisorder-dev

Recommends: libssw-dev, libssw-java

Recommends: libfml-dev

Recommends: libgkarrays-dev

Recommends: libjloda-java

Recommends: libvcflib-dev

Recommends: libswiss-perl

Recommends: python3-pybedtools

Recommends: python3-gffutils

Recommends: python3-bx | python-bx
WNPP: 851242

Recommends: python-bd2k, toil, galaxy-lib

Recommends: libbio-eutilities-perl

Recommends: libseqlib-dev

Recommends: libroadrunner-dev, python-roadrunner

Recommends: python3-biotools | python-biotools

Recommends: bio-tradis

Recommends: python3-biomaj3 | python-biomaj3

Suggests: python-pyflow

Suggests: r-cran-natserv

Recommends: libbiod-dev

Recommends: python3-ete3 | python-ete3

Recommends: python3-gfapy

Recommends: libgoby-java

Recommends: python3-gffutils, python3-pybedtools

Recommends: python3-seqcluster

Recommends: python3-cyvcf2

Recommends: libgatbcore-dev

Suggests: libmilib-java
